$(document).ready(function () {


    /*$("input[type='file']").on('change', (function (e) {

        const file = e.target.files[0];
        const size = file.size;
        const type = file.type;

        const extensions = ['image/png'];

        if (!extensions.includes(type)) {

            $("#err").text('type incorrect');
            return false;
        }

        // 10MO => 10485760
        else if (size > 10485760) {
            $("#err").text('size incorrect')
            return false;
        }

        else {
            $("#err").text('');
        }


        var formData = new FormData();
        formData.append('image', file);


        $.ajax({
            url: "ajaxUpload.php",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $("#err").html('');
            },
            success: function (data) {
                if (data == 'invalid') {
                    $("#err").html("Invalid File !").fadeIn();
                }
                else {
                    $("#preview").html(data).fadeIn();
                    $("#form")[0].reset();
                }
            },
            error: function (e) {
                $("#err").html(e).fadeIn();
            }
        });
    }));*/


    $("input[type='file']").on('change', (function (e) {

        const file = e.target.files[0];
        const fileName = file.name;
        const size = file.size;
        const sizeMegaBits = Math.ceil(size / 1_048_576);

        $('.i-upload span').text(fileName);
        $('.progress-start').text(sizeMegaBits + 'MO');


        let formData = new FormData();
        formData.append('image', file);


        $.ajax({
            url: "ajaxUpload.php",
            type: "POST",
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () { },
            success: function (data) {

                if (data.status === 200) {

                    $("#form")[0].reset();
                } else {
                    $('.pupop').addClass('open');
                }


                c = false
            },
            error: function (e) { },
            complete: function () {

                var index = 0;

                if (index == 0) {

                    let timerProgress = setInterval(frame, 10);
                    let step = sizeMegaBits / 100;
                    let i = step;
                    let width = 0;

                    function frame() {
                        if (width >= 100) {
                            clearInterval(timerProgress);
                            index = 0;
                            i = step;
                        } else {
                            width++;
                            i += step;
                            $('.bar').css("width", width + "%");
                            $('.progress-end').text(' / ' + Math.round(i) + 'MO');
                        }
                    }
                }
            }



        });






    }));
});