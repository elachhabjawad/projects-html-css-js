<?php


define('HOST', 'localhost');
define('DBNAME', 'uploader_files');
define('USERNAME', 'root');
define('PASSWORD', '');


$dsn = "mysql:host=" . HOST . ";dbname=" . DBNAME . ";charset=UTF8";

try {
    $pdo = new PDO($dsn, USERNAME, PASSWORD);

    $db = $pdo;
    if ($pdo) {
        echo "Connected to the database successfully!";
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}

